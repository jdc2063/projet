const express = require('express');
const app = express.Router();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017/';
const dbName = 'paysApi';
let db;
 
MongoClient.connect(url, function(err, client) {
  console.log("Connect to Database mongodb/Capital: ok");
  db = client.db(dbName);
});

app.route('/')
  .get(function(req, res) {
    db.collection('ocean').find({}).sort({name: 1}).toArray(function(err, docs) {
        if (err) {
            console.log(err);
            throw err;
        }
        res.status(200).json(docs);
      }) 
  })
  .post( async (req, res) => {
    var name = req.body.name,
      size = parseInt(req.body.size),
      depth = parseInt(req.body.depth)
    try {
      await db.collection('ocean').insertOne({ name: name, size: size, depth: depth});
      res.status(201).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  });

  app.put('/:name', async (req, res) => {
    const name_url = req.params.name;
    var name = req.body.name,
      size = parseInt(req.body.size),
      depth = parseInt(req.body.depth)
    try {
      await db.collection('ocean').updateOne({name: name_url}, {$set: {name: name, size: size, depth: depth} });
      res.status(200).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  })
  
  app.delete('/:name', async (req, res) => {
    const name = req.params.name;
    try {
      await db.collection('ocean').deleteOne({name: name});
      res.status(200).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  })
  

app.get('/:name', (req, res) => {
    db.collection('ocean').findOne({name: req.params.name}, function(err, docs) {
      res.status(200).json(docs);
    });
})

//faire + ou - de la valeur entrée
app.get('/superficie/:size', (req, res) => {
    db.collection('ocean').find({size: {$lte: parseInt(req.params.size)}}).sort({name: 1}).toArray(function(err, docs) {
      res.status(200).json(docs);
    });
})

app.get('/profondeur/:depth', (req, res) => {
    db.collection('ocean').find({depth: {$lte: parseInt(req.params.depth)}}).sort({name: 1}).toArray(function(err, docs) {
      res.status(200).json(docs);
    });  
})

module.exports = app;

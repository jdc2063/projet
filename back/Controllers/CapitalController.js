const express = require('express');
const app = express.Router();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017/';
const dbName = 'paysApi';
let db;
 
MongoClient.connect(url, function(err, client) {
  console.log("Connect to Database mongodb/Capital: ok");
  db = client.db(dbName);
});

app.route('/')
  .get(function(req, res) {
    db.collection('capital').find({}).sort({name: 1}).toArray(function(err, docs) {
        if (err) {
            console.log(err);
            throw err;
        }
        res.status(200).json(docs);
      }) 
  })
  .post( async (req, res) => {
    var name = req.body.name,
      mayor = req.body.mayor,
      people_nbr = parseInt(req.body.people_nbr),
      size = parseInt(req.body.size),
      time_zone = req.body.time_zone
    try {
      await db.collection('capital').insertOne({ name: name, mayor: mayor, people_nbr: people_nbr, size: size, time_zone: time_zone});
      res.status(201).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  });

  app.put('/:name', async (req, res) => {
    const name_url = req.params.name;
    var name = req.body.name,
      mayor = req.body.mayor,
      people_nbr = parseInt(req.body.people_nbr),
      size = parseInt(req.body.size),
      time_zone = req.body.time_zone
    try {
      await db.collection('capital').updateOne({name: name_url}, {$set: {name: name, mayor: mayor, people_nbr: people_nbr, size: size, time_zone: time_zone} });
      res.status(200).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  })
  
  app.delete('/:name', async (req, res) => {
    const name = req.params.name;
    try {
      await db.collection('capital').deleteOne({name: name});
      res.status(200).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  })
  

app.get('/:name', (req, res) => {
    db.collection('capital').findOne({name: req.params.name}, function(err, docs) {
      res.status(200).json(docs);
    });
})

app.get('/horaire/:time', (req, res) => {
    db.collection('capital').find({time_zone: req.params.time}).sort({name: 1}).toArray(function(err, docs) {
        if (err) {
            console.log(err);
            throw err;
        }
        res.status(200).json(docs);
      }) 
})

//faire + ou - de la valeur entrée
app.get('/superficie/:size', (req, res) => {
    db.collection('capital').find({size: {$lte: parseInt(req.params.size)}}).sort({name: 1}).toArray(function(err, docs) {
      res.status(200).json(docs);
    });
})

app.get('/habitants/:people', (req, res) => {
    db.collection('capital').find({people_nbr: {$lte: parseInt(req.params.people)}}).sort({name: 1}).toArray(function(err, docs) {
      res.status(200).json(docs);
    });  
})

module.exports = app;

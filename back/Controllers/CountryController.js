const express = require('express');
const app = express.Router();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017/';
const dbName = 'paysApi';
let db;
 
MongoClient.connect(url, function(err, client) {
  console.log("Connect to Database mongodb/country: ok");
  db = client.db(dbName);
});

app.route('/')
  .get(function(req, res) {
    db.collection('country').find({}).sort({name: 1}).toArray(function(err, docs) {
        if (err) {
            console.log(err);
            throw err;
        }
        res.status(200).json(docs);
      }) 
  })
  .post( async (req, res) => {
    var name = req.body.name,
      executive = req.body.executive,
      people_nbr = parseInt(req.body.people_nbr),
      size = parseInt(req.body.size),
      continent = req.body.continent,
      capital = req.body.capital,
      telephone = req.body.telephone;
    try {
      await db.collection('country').insertOne({ name: name, executive: executive, people_nbr: people_nbr, size:size, continent: continent, capital: capital, telephone: telephone});
      res.status(201).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  });

  app.put('/:name', async (req, res) => {
    const name_url = req.params.name;
    var name = req.body.name,
    dirigeant = req.body.dirigeant,
    people_nbr = parseInt(req.body.people_nbr),
    size = parseInt(req.body.size),
    continent = req.body.continent,
    capital = req.body.capital,
    telephone = req.body.telephone;
    try {
      await db.collection('country').updateOne({name: name_url}, {$set: {name: name, dirigeant: dirigeant, people_nbr: people_nbr, size:size, continent: continent, capital: capital, telephone: telephone} });
      res.status(200).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  })
  
  app.delete('/:name', async (req, res) => {
    const name = req.params.name;
    try {
      await db.collection('country').deleteOne({name: name});
      res.status(200).json("ok");
    } catch (err) {
      console.log(err);
      throw err;
    }
  })
  

app.get('/:name', (req, res) => {
    db.collection('country').findOne({name: req.params.name}, function(err, docs) {
      res.status(200).json(docs);
    });
})

app.get('/continent/:name', (req, res) => {
    db.collection('country').find({continent: req.params.name}).sort({name: 1}).toArray(function(err, docs) {
        if (err) {
            console.log(err);
            throw err;
        }
        res.status(200).json(docs);
      }) 
})

app.get('/superficie/:size', (req, res) => {
    db.collection('country').find({size: {$lte: parseInt(req.params.size)}}).sort({name: 1}).toArray(function(err, docs) {
      res.status(200).json(docs);
    });
        
})

app.get('/habitants/:people', (req, res) => {
    db.collection('country').find({people_nbr: {$lte: parseInt(req.params.people)}}).sort({name: 1}).toArray(function(err, docs) {
      res.status(200).json(docs);
    });  
})

app.get('/telephone/:telephone', (req, res) => {
  db.collection('country').findOne({telephone: req.params.telephone}, function(err, docs) {
    res.status(200).json(docs);
  });
})

app.get('/capitale/:name', (req, res) => {
  db.collection('country').findOne({capital: req.params.name}, function(err, docs) {
    res.status(200).json(docs);
  });  
})


module.exports = app;

const express = require('express')
const app = express()
const port = process.env.PORT || 3000;

require('./models/CapitalModel.js');
require('./models/CountryModel.js');
require('./models/OceanModel.js');

var pays = require('./Controllers/CountryController.js');
app.use('/pays', pays);

var capitale = require('./Controllers/CapitalController.js');
app.use('/capitale', capitale);

var ocean = require('./Controllers/OceanController.js');
app.use('/ocean', ocean);

app.listen(port, () => {
  console.log(`http://localhost:${port}`)
  console.log(`Connect to Node.js server: ok`)
})
